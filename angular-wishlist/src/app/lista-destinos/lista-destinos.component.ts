import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
destinos:DestinoViaje[];
dest:string[];

  constructor() { 
  this.destinos=[];
  this.dest=['Barranquilla', 'Lima', 'Buenos Aires', 'La Paz'];
  }
  ngOnInit(): void{
  }
  guardar(nombre:string, url:string): boolean{
  this.destinos.push(new DestinoViaje (nombre,url));
  return false;
  }

}
